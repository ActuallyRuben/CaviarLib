meta:
  id: caviar
  file-extension: cvr
  endian: le
  bit-endian: le
seq:
  - id: root
    type: chunk

types:
  chunk:
    seq:
      - id: tag
        type: u4
        enum: chunk_type
      - id: chunk_size
        type: u4
      - id: data
        size: chunk_size - 8
        type:
          switch-on: tag
          cases:
            'chunk_type::file_container': file_container
            'chunk_type::version': number
            'chunk_type::db_name': string
            'chunk_type::palette_container': palette
            'chunk_type::palette_name': string
            'chunk_type::palette_data': palette_data
            'chunk_type::palette_material': palette_material
            'chunk_type::scene_container': scene_container
            'chunk_type::scene_name': string
            'chunk_type::object_counter': number
            'chunk_type::frame_counter': number
            'chunk_type::object_container': object_container
            'chunk_type::object_name': string
            'chunk_type::geometry_container': geometry_container
            'chunk_type::voxel_object': voxel_object
            'chunk_type::animation_container': animation_container
            'chunk_type::object_flag': object_flag
            'chunk_type::object_location': object_location
            'chunk_type::object_matrix': object_matrix
  file_container:
    seq:
      - id: version
        type: chunk
      - id: db_name
        type: chunk
      - id: palette
        type: chunk
      - id: scene
        type: chunk
  number:
    seq:
      - id: number
        type: u4
  string:
    seq:
      - id: name
        type: str
        encoding: ascii
        size-eos: true
  palette:
    seq:
      - id: name
        type: chunk
      - id: data
        type: chunk
      - id: material
        type: chunk
        repeat: eos
  palette_data:
    seq:
      - id: first_color_index
        type: u1
      - id: last_color_index
        type: u1
      - id: colors
        type: rgb
        repeat: expr
        repeat-expr: 0x100
      - id: shade
        type: u1
        repeat: expr
        repeat-expr: 0x1800
  palette_material: # TODO: Find a file that uses materials
    seq:
      - id: colors
        type: r5g5b5
        repeat: expr
        repeat-expr: 0x1800
  rgb:
    seq:
      - id: r
        type: u1
      - id: g
        type: u1
      - id: b
        type: u1
  r5g5b5:
    seq:
      - id: r
        type: b5
      - id: g
        type: b5
      - id: b
        type: b5
      - type: b1
      
  scene_container:
    seq:
      - id: name
        type: chunk
      - id: children
        type: chunk
        repeat: eos
  object_container:
    seq:
      - id: name
        type: chunk
      - id: children
        type: chunk
        repeat: eos
  geometry_container:
    seq:
      - id: voxel_object
        type: chunk
  voxel_object:
    seq:
      - id: header
        type: voxel_header
  
  voxel_header:
    seq:
      - id: multiple_headers  # TODO: find file with multiple headers
        type: b1
      - id: jump_headers
        type: b1
      - id: flags
        type: b30
      - id: object_size
        type: u4
      - id: units_per_voxel
        type: f4
      - id: block_size
        type: u4
      - id: end_of_block
        type: u4
      - id: scale
        type: int_vector
      - id: position
        type: int_vector
      - id: rotation_center
        type: int_vector
      - id: block_voxel_count
        type: u4
      - id: voxel_data
        type: voxel_data
        repeat: expr
        repeat-expr: block_voxel_count
        if: jump_headers == false
      - id: jump_header
        type: jump_header(block_voxel_count)
        if: jump_headers == true
  jump_header:
    params:
      - id: remaining_voxels
        type: u4
    seq:
      - id: position
        type: int_vector
      - id: unknown1
        type: int_vector
      - id: unknown2
        type: int_vector
      - id: position_offset
        type: int_vector
      - id: block_voxel_count
        type: u4
      - id: has_offset
        type: b1
      - id: flags
        type: b31
      - id: next_header_offset
        type: u4
        if: has_offset == true
      - id: voxel_data
        type: voxel_data
        repeat: expr
        repeat-expr: block_voxel_count
      - id: next_jump_header
        type: jump_header(next_remaining_voxels)
        if: next_remaining_voxels > 0
    instances:
      next_remaining_voxels:
        value: remaining_voxels - block_voxel_count
        
  voxel_data:
    seq:
      - id: direction
        type: u1
      - id: normal
        type: u1
      - id: color
        type: u1
  int_vector:
    seq:
      - id: z
        type: s2
      - id: x
        type: s2
      - id: y
        type: s2
  animation_container:
    seq:
      - id: children
        type: chunk
        repeat: eos
  object_flag:
    seq:
      - id: flags
        type: u1
        repeat: eos
  object_location:
    seq:
      - id: locations
        type: vector
        repeat: eos
  vector:
    seq:
      - id: z
        type: f4
      - id: x
        type: f4
      - id: y
        type: f4
  object_matrix:
    seq:
      - id: matrices
        type: matrix
        repeat: eos
  matrix:
    seq:
      - id: n
        type: f4
        repeat: expr
        repeat-expr: 9

enums:
  chunk_type:
    0x00525643: file_container
    0x01000000: version
    0x02000000: db_name
    
    0x03000000: palette_container
    0x01010000: palette_name
    0x01020000: palette_data
    0x01030000: material
    
    0x04000000: scene_container
    0x04010000: scene_name
    0x04020000: object_counter
    0x04030000: frame_counter
    
    0x04040000: object_container
    0x04040100: object_name
    
    0x04040200: geometry_container
    0x04040201: voxel_object
    
    0x04040300: animation_container
    0x04040301: object_flag
    0x04040302: object_location
    0x04040303: object_matrix